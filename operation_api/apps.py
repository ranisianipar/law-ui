from django.apps import AppConfig


class OperationApiConfig(AppConfig):
    name = 'operation_api'
