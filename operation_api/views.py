from django.shortcuts import render
from django.http import JsonResponse

def add(request):
    response = {}
    if request.method == 'GET':
        num1 = request.GET.get('num1', 0)
        num2 = request.GET.get('num2', 0)
        try:
            response['result'] = int(num1) + int(num2)
        except:
            response['error'] = 'wrong input!'
    return JsonResponse(response)

def multiple(request):
    response = {}
    if request.method == 'GET':
        num1 = request.GET.get('num1', 0)
        num2 = request.GET.get('num2', 0)
        try:
            response['result'] = int(num1) * int(num2)
        except:
            response['error'] = 'wrong input!'
    return JsonResponse(response)
