from django.conf.urls import url
from imdb_service import views

urlpatterns = [
    url(r'^$', views.index, name='imdb_list'),
]
