from django.shortcuts import render
from django.conf import settings
import requests


IMDB_API = "http://www.omdbapi.com/?apikey={}&t={}"
API_KEY = settings.IMDB_API_KEY
search_key = ""
response = {}
def index(request):
    search_key = request.GET.get('search_title', '')
    api_result = requests.get(IMDB_API.format(API_KEY, search_key)).json()
    
    response = {'title':'-', 'year':'-', 'released': '-', 'genre':'-'}
    if api_result['Response'].lower() == 'true':
        response['title'] = api_result['Title']
        response['year'] = api_result['Year']
        response['released'] = api_result['Released']
        response['genre'] = api_result['Genre']

    return render(request, 'imdb_service/movie.html', {
        'movie': response
    })