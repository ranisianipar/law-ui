from django.apps import AppConfig


class ImdbServiceConfig(AppConfig):
    name = 'imdb_service'
